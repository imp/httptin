use hyper::server::{Request, Response};

pub fn handler(request: &Request, mut _response: Response) {
    info!(
        "** Handling POST {} from {}",
        request.uri, request.remote_addr
    );
    info!("** Incoming headers {:?}", request.headers);
}
