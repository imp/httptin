extern crate hyper;
extern crate itertools;
#[macro_use]
extern crate serde_derive;
extern crate pretty_env_logger;
extern crate serde_json;
#[macro_use]
extern crate log;

use hyper::header;
use hyper::server::{Handler, Request, Response, Server};
use hyper::status::StatusCode;
use hyper::{Get, Post};

mod get;
mod makeresponse;
mod post;

struct HttpTin {
    server: String,
}

impl HttpTin {
    pub fn new() -> Self {
        let server = format!("{}/{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
        HttpTin { server }
    }

    fn prepare_response(&self, response: &mut Response) {
        let server = header::Server(self.server.clone());
        response.headers_mut().set(server);
    }
}

impl Handler for HttpTin {
    fn handle(&self, request: Request, mut response: Response) {
        self.prepare_response(&mut response);
        match request.method {
            Get => get::handler(&request, response),
            Post => post::handler(&request, response),
            _ => *response.status_mut() = StatusCode::MethodNotAllowed,
        }
    }
}

fn main() {
    pretty_env_logger::init();
    let httptin = HttpTin::new();
    let server = Server::http("localhost:8000").unwrap();
    if let Ok(active) = server.handle(httptin) {
        info!("{:?}", active);
    }
}
